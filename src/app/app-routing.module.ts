import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuicklinkStrategy } from 'ngx-quicklink';

const routes: Routes = [
  {
    path: 'first',
    loadChildren: () => import('./pages/first-module/first-module.module').then(m => m.FirstModuleModule),
  },
  {
    path: 'second',
    loadChildren: () => import('./pages/second-module/second-module.module').then(m => m.SecondModuleModule),
  },
  {
    path: 'third',
    loadChildren: () => import('./pages/third-module/third-module.module').then(m => m.ThirdModuleModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        preloadingStrategy: QuicklinkStrategy,
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
