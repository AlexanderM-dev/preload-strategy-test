import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThirdSecondComponent } from './third-second.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ThirdSecondComponent,
  }
]

@NgModule({
  declarations: [
    ThirdSecondComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ]
})
export class ThirdSecondModule { }
