import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThirdFirstComponent } from './third-first.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ThirdFirstComponent,
  }
]

@NgModule({
  declarations: [
    ThirdFirstComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
  ]
})
export class ThirdFirstModule { }
