import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ThirdModuleComponent } from './third-module.component';


@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ThirdModuleComponent,
        children: [
          {
            path: 'third-first',
            loadChildren: () => import('./third-first/third-first.module').then(m => m.ThirdFirstModule),
          },
          {
            path: 'third-second',
            loadChildren: () => import('./third-second/third-second.module').then(m => m.ThirdSecondModule),
          },
        ]
      },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class ThirdModuleRoutingModule { }
