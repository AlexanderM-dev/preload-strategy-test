import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThirdModuleComponent } from './third-module.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThirdModuleRoutingModule } from './third-module-routing.module';

@NgModule({
  declarations: [
    ThirdModuleComponent
  ],
  imports: [
    CommonModule,
    ThirdModuleRoutingModule,
    SharedModule,
  ]
})
export class ThirdModuleModule { }
