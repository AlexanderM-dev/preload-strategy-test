import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecondModuleComponent } from './second-module.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: SecondModuleComponent,
  },
];

@NgModule({
  declarations: [
    SecondModuleComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SecondModuleModule { }
